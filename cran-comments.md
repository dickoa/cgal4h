## Test environments

* ubuntu 18.04 (on gitlab-ci), R 4.0.0
* win-builder (devel and release)

## R CMD check results

❯ checking installed package size ... NOTE
    installed size is 37.8Mb
    sub-directories of 1Mb or more:
      include  37.8Mb

0 errors | 0 warnings | 1 note


## Submission 1

This is the first release of cgal4h and this submission fix the issue with the version number.
I still have a package with a size greater than 5Mb but it is the size of the header files of CGAL 4 this package provides.

Thanks!
Ahmadou Dicko

## Submission 3

Dear CRAN team,

This is a the third submission of the first release of CGAL 4 headers package. You will find below some of the change I made since the last submission.

> Thanks, can you add a web link for the included CGAL library in the
> form https://.....?

Link of the CGAL library were added to Description.

> We see you mix up the terms copyright, license and authors.
> Apparently the contents is written by many authors, so please give a
> complete listing somewhere, if too long for Authors@R, say so in that
> field and point to a separate file.
> Same is true for copyright holders.
>
> In the COPYRIGHT file you are providing a lot os copyright hodlers are
> missing.
>
> A listing od authors and coiyright holders we tried to collect starts

An AUTHORS text file in the inst/ folder with the full list of authors was created. A code to get all the names and check copyright holder was written and used to verify the list of authors.

The license of the cgal4h package was changed from GPL-3 | LGPL-3 to GPL-3 | LICENSE file to make it more explicit. Further information are available in inst/COPYRIGHT and LICENSE file about the compatibility of non GPL/LGPL files with the GPL-3 license. No file without explicit license is shared with cgal4h therefore there is no use of th FREE USE LICENSE.

Thanks!
Ahmadou Dicko
